const Icono = (props) => {
    return(
        <div className="sug-image-padre d-flex justify-content-between">
                            
            <div className="sug-image d-flex align-items-center">
                <img alt="asdf"src={props.imagen} style={{ width:'100%', borderRadius:'50%'}}></img>
            </div>

            <div className="d-flex align-items-center" style={{width: '45%'}}>
                <div>
                    <p className="cont-sug m-0">{props.nombre}</p>
                </div>
            </div>

            <div style={{width: '25%',  display: 'flex', alignItems: 'center',  justifyContent: 'center'}}>
                <button className="btn btn-primary mr-3 w-100"> <b> Ver mas </b></button>
            </div>
        </div>
    )
}
export default Icono