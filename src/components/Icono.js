const Icono = (props) => {
    return(
        <div className="col-3 header-bottom-content">
            <div className="class">
                <div className="header-img">
                    <svg className="svg-inline--fa imagen" role="img" xmlns="http://www.w3.org/2000/svg" viewBox={props.viewbox}><path fill="currentColor" d={props.d}></path></svg>
                </div>

                <div>
                    <p className="text-center">{props.p}</p>
                </div>

            </div>
        </div>
    )
}
export default Icono