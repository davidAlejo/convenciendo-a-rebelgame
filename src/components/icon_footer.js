const IconFooter = (props) => {
    return(

        <div className="footer col-3">
            <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="home" className="svg-inline--fa fa-home fa-w-18" role="img" xmlns="http://www.w3.org/2000/svg" viewBox={props.viewBox}><path fill="currentColor" d={props.d}></path></svg>
        </div>

    )
}
export default IconFooter