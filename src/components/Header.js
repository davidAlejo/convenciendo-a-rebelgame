const Header = (props) => {
    return (
        <div className="row header-top align-items-center">
            <div className="col-10 header-title">
                <h3>{props.titulo}</h3>
            </div>
            <div className="col-2 header-campana">
                <img src={props.imagen} alt="asdf"/>
            </div>
        </div>
    )
}

export default Header