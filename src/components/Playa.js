const Playa = (props) => {
    return(
        <div className="places" style={{border:'10px solid', padding:'5px', flex: '0 0 45%', marginBottom: '17px'}}>
            <img alt="asdf" src={props.imagen} width="100%" height="100%"/>
            <div style={{marginTop: '-50px'}}>
                <p className="text-center" style={{marginBottom: '0px', color: 'white', position: 'relative'}}><b>{props.titulo}</b></p>
                <p style={{marginBottom: '0px', marginLeft: '6px',color: 'white', position: 'relative'}}>
                    <svg aria-hidden="true" style={{width: '0.8rem', marginRight: '4px'}} focusable="false" data-prefix="fas" data-icon="map-marker-alt" className="svg-inline--fa fa-map-marker-alt fa-w-12" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512"><path fill="currentColor" d="M172.268 501.67C26.97 291.031 0 269.413 0 192 0 85.961 85.961 0 192 0s192 85.961 192 192c0 77.413-26.97 99.031-172.268 309.67-9.535 13.774-29.93 13.773-39.464 0zM192 272c44.183 0 80-35.817 80-80s-35.817-80-80-80-80 35.817-80 80 35.817 80 80 80z"></path></svg>
                    {props.ubicacion}
                </p>
            </div>
        </div>
    )
}
export default Playa