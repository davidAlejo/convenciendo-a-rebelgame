import Icono from './Icono'
import Header from './Header'
import Playa from './Playa'
import Sugerencia from './Sugerencia'
import IconFooter from './icon_footer'

const App = () => {
    return(
        <div className="body">
            <header>
                <div className="container">

                    <Header titulo="¿A donde quieres ir?" imagen="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAABmJLR0QA/wD/AP+gvaeTAAACLUlEQVRoge2ZMWsUQRiGn5lNUghRcyDqJqnsVLC1TXJoZ6WJjR6JSSVoZ2JjQkRQbOw3B4qFcCpYJ6v5E1qIYOPdIQg5snYmu2OTiMWeuzM7m0UyTzkz7/d+L9/NcbcLDsfhRpRR9FyrNdSrRQ+BG4ACXo5sHX3waXr6l22vAdsFAXq1aBW499fSYq8WASzZ9pK2C+5xM2WtUYZRWQFOp6ydKsPI6h0YX2/6u1JdE/As/YS6u+MNtH5MzH635WklwOhmMKZ25SpCNcieagzquRRDy+2pRqeod+EA/ofmFRL1AjiuKf0pUHOd+sKbIv6F7oAfBndI1Dv0mwcYVoiWHwa3i/RgPAE/DGZAvCpSY49EoGZMJ2FkPr7e9GOpPgIjJvoUtoWnzncmFtq6QqOPUOypp9hrHuBYEvPIRKg9gdHNYEzF4iswaGL4D3aI5Znu5blvOiLtCahEXMd+8wCDQiZXdUXaAYRiUleTFyWY0tXoTwDO6mo00K5tcolPGGjyclJXYBLgiIGmtNpl/Ro9MFyAqnEBqsYFqBoXoGpcgKrRCuBvBCtlNfLHI1x7rHM+9z8yfyNYQYhl/ZaMeNKtz+d6jporwAE3v0+uEJkBKmp+n8wQ2XeguuYBFrMO/PffQo6qsf6OzA/XImC4z/Z2tz5v8iC4L2Xcgff9NgSEts3sB5BiCeil7GyJxLtv3c52we7krc/E8gLwGoiASMBbmXgX25dmv9j2czgOO78BEMWFxDn/5T8AAAAASUVORK5CYII="/>

                    <hr style={{border: '2px solid black',boxShadow: '1px 1px 10px #', marginTop:'10px' }} />

                    <div className="row header-bottom">
                        
                        <Icono p="Tour1" d="M336 160H48c-26.51 0-48 21.49-48 48v224c0 26.51 21.49 48 48 48h16v16c0 8.84 7.16 16 16 16h32c8.84 0 16-7.16 16-16v-16h128v16c0 8.84 7.16 16 16 16h32c8.84 0 16-7.16 16-16v-16h16c26.51 0 48-21.49 48-48V208c0-26.51-21.49-48-48-48zm-16 216c0 4.42-3.58 8-8 8H72c-4.42 0-8-3.58-8-8v-16c0-4.42 3.58-8 8-8h240c4.42 0 8 3.58 8 8v16zm0-96c0 4.42-3.58 8-8 8H72c-4.42 0-8-3.58-8-8v-16c0-4.42 3.58-8 8-8h240c4.42 0 8 3.58 8 8v16zM144 48h96v80h48V48c0-26.51-21.49-48-48-48h-96c-26.51 0-48 21.49-48 48v80h48V48z" viewbox="0 0 384 512"/>
                        <Icono p="Flight" d="M480 192H365.71L260.61 8.06A16.014 16.014 0 0 0 246.71 0h-65.5c-10.63 0-18.3 10.17-15.38 20.39L214.86 192H112l-43.2-57.6c-3.02-4.03-7.77-6.4-12.8-6.4H16.01C5.6 128-2.04 137.78.49 147.88L32 256 .49 364.12C-2.04 374.22 5.6 384 16.01 384H56c5.04 0 9.78-2.37 12.8-6.4L112 320h102.86l-49.03 171.6c-2.92 10.22 4.75 20.4 15.38 20.4h65.5c5.74 0 11.04-3.08 13.89-8.06L365.71 320H480c35.35 0 96-28.65 96-64s-60.65-64-96-64z" viewbox="0 0 576 512"/>
                        <Icono p="Train" d="M448 96v256c0 51.815-61.624 96-130.022 96l62.98 49.721C386.905 502.417 383.562 512 376 512H72c-7.578 0-10.892-9.594-4.957-14.279L130.022 448C61.82 448 0 403.954 0 352V96C0 42.981 64 0 128 0h192c65 0 128 42.981 128 96zM200 232V120c0-13.255-10.745-24-24-24H72c-13.255 0-24 10.745-24 24v112c0 13.255 10.745 24 24 24h104c13.255 0 24-10.745 24-24zm200 0V120c0-13.255-10.745-24-24-24H272c-13.255 0-24 10.745-24 24v112c0 13.255 10.745 24 24 24h104c13.255 0 24-10.745 24-24zm-48 56c-26.51 0-48 21.49-48 48s21.49 48 48 48 48-21.49 48-48-21.49-48-48-48zm-256 0c-26.51 0-48 21.49-48 48s21.49 48 48 48 48-21.49 48-48-21.49-48-48-48z" viewbox="0 0 448 512"/>
                        <Icono p="Hotel" d="M560 64c8.84 0 16-7.16 16-16V16c0-8.84-7.16-16-16-16H16C7.16 0 0 7.16 0 16v32c0 8.84 7.16 16 16 16h15.98v384H16c-8.84 0-16 7.16-16 16v32c0 8.84 7.16 16 16 16h240v-80c0-8.8 7.2-16 16-16h32c8.8 0 16 7.2 16 16v80h240c8.84 0 16-7.16 16-16v-32c0-8.84-7.16-16-16-16h-16V64h16zm-304 44.8c0-6.4 6.4-12.8 12.8-12.8h38.4c6.4 0 12.8 6.4 12.8 12.8v38.4c0 6.4-6.4 12.8-12.8 12.8h-38.4c-6.4 0-12.8-6.4-12.8-12.8v-38.4zm0 96c0-6.4 6.4-12.8 12.8-12.8h38.4c6.4 0 12.8 6.4 12.8 12.8v38.4c0 6.4-6.4 12.8-12.8 12.8h-38.4c-6.4 0-12.8-6.4-12.8-12.8v-38.4zm-128-96c0-6.4 6.4-12.8 12.8-12.8h38.4c6.4 0 12.8 6.4 12.8 12.8v38.4c0 6.4-6.4 12.8-12.8 12.8h-38.4c-6.4 0-12.8-6.4-12.8-12.8v-38.4zM179.2 256h-38.4c-6.4 0-12.8-6.4-12.8-12.8v-38.4c0-6.4 6.4-12.8 12.8-12.8h38.4c6.4 0 12.8 6.4 12.8 12.8v38.4c0 6.4-6.4 12.8-12.8 12.8zM192 384c0-53.02 42.98-96 96-96s96 42.98 96 96H192zm256-140.8c0 6.4-6.4 12.8-12.8 12.8h-38.4c-6.4 0-12.8-6.4-12.8-12.8v-38.4c0-6.4 6.4-12.8 12.8-12.8h38.4c6.4 0 12.8 6.4 12.8 12.8v38.4zm0-96c0 6.4-6.4 12.8-12.8 12.8h-38.4c-6.4 0-12.8-6.4-12.8-12.8v-38.4c0-6.4 6.4-12.8 12.8-12.8h38.4c6.4 0 12.8 6.4 12.8 12.8v38.4z" viewbox="0 0 576 512"/>
                        
                    </div>

                    <hr style={{border: '2px solid black',boxShadow: '7px 7px 15px #592A08', marginTop:'20px' }} />

                </div>
            </header>
            
            <main>
                <div className="container mt-4 mb-5 pb-2 pt-4">

                    <div>
                        <p><b>Playas de estados Unidos EE.UU.</b></p>
                    </div>             

                    <div className="d-flex justify-content-between" style={{width: '100%', flexWrap: 'wrap'}}>
                        <Playa imagen="https://cruisewhitsundays.imgix.net/2019/03/CWS-Whitehaven-Beach-Aerial-View-of-Hill-Inlet-The-Whitsundays-Tourism-Queensland_1920_1920.jpg?fit=crop&w=500&h=595&dpr=2.625&q=25&auto=format" titulo="Bali Beach" ubicacion="es mi ubicacion"/>
                        <Playa imagen="https://www.ithaka.travel/blog/wp-content/uploads/2018/08/balangan_beach.jpg" titulo="playa2" ubicacion="es mi ubicacion"/>
                        
                    </div>

                    <div className="mt-2">
                        <p><b>Sugerencias para Ti</b></p>
                    </div>

                    <div className="sugerencias d-flex mb-3">
                        
                        <Sugerencia imagen="https://upload.wikimedia.org/wikipedia/commons/6/65/Mancorabeach1.jpg" nombre="Aguas Calientes" />

                    </div>

                </div>
            </main>

            <footer>
                <div className="footer-padre row">
                <IconFooter d="M280.37 148.26L96 300.11V464a16 16 0 0 0 16 16l112.06-.29a16 16 0 0 0 15.92-16V368a16 16 0 0 1 16-16h64a16 16 0 0 1 16 16v95.64a16 16 0 0 0 16 16.05L464 480a16 16 0 0 0 16-16V300L295.67 148.26a12.19 12.19 0 0 0-15.3 0zM571.6 251.47L488 182.56V44.05a12 12 0 0 0-12-12h-56a12 12 0 0 0-12 12v72.61L318.47 43a48 48 0 0 0-61 0L4.34 251.47a12 12 0 0 0-1.6 16.9l25.5 31A12 12 0 0 0 45.15 301l235.22-193.74a12.19 12.19 0 0 1 15.3 0L530.9 301a12 12 0 0 0 16.9-1.6l25.5-31a12 12 0 0 0-1.7-16.93z" viewBox="0 0 576 512" />

                <IconFooter d="M505 442.7L405.3 343c-4.5-4.5-10.6-7-17-7H372c27.6-35.3 44-79.7 44-128C416 93.1 322.9 0 208 0S0 93.1 0 208s93.1 208 208 208c48.3 0 92.7-16.4 128-44v16.3c0 6.4 2.5 12.5 7 17l99.7 99.7c9.4 9.4 24.6 9.4 33.9 0l28.3-28.3c9.4-9.4 9.4-24.6.1-34zM208 336c-70.7 0-128-57.2-128-128 0-70.7 57.2-128 128-128 70.7 0 128 57.2 128 128 0 70.7-57.2 128-128 128z" viewBox="0 0 512 512" />

                <IconFooter d="M487.4 315.7l-42.6-24.6c4.3-23.2 4.3-47 0-70.2l42.6-24.6c4.9-2.8 7.1-8.6 5.5-14-11.1-35.6-30-67.8-54.7-94.6-3.8-4.1-10-5.1-14.8-2.3L380.8 110c-17.9-15.4-38.5-27.3-60.8-35.1V25.8c0-5.6-3.9-10.5-9.4-11.7-36.7-8.2-74.3-7.8-109.2 0-5.5 1.2-9.4 6.1-9.4 11.7V75c-22.2 7.9-42.8 19.8-60.8 35.1L88.7 85.5c-4.9-2.8-11-1.9-14.8 2.3-24.7 26.7-43.6 58.9-54.7 94.6-1.7 5.4.6 11.2 5.5 14L67.3 221c-4.3 23.2-4.3 47 0 70.2l-42.6 24.6c-4.9 2.8-7.1 8.6-5.5 14 11.1 35.6 30 67.8 54.7 94.6 3.8 4.1 10 5.1 14.8 2.3l42.6-24.6c17.9 15.4 38.5 27.3 60.8 35.1v49.2c0 5.6 3.9 10.5 9.4 11.7 36.7 8.2 74.3 7.8 109.2 0 5.5-1.2 9.4-6.1 9.4-11.7v-49.2c22.2-7.9 42.8-19.8 60.8-35.1l42.6 24.6c4.9 2.8 11 1.9 14.8-2.3 24.7-26.7 43.6-58.9 54.7-94.6 1.5-5.5-.7-11.3-5.6-14.1zM256 336c-44.1 0-80-35.9-80-80s35.9-80 80-80 80 35.9 80 80-35.9 80-80 80zz" viewBox="0 0 512 512" />

                <IconFooter d="M313.6 304c-28.7 0-42.5 16-89.6 16-47.1 0-60.8-16-89.6-16C60.2 304 0 364.2 0 438.4V464c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48v-25.6c0-74.2-60.2-134.4-134.4-134.4zM400 464H48v-25.6c0-47.6 38.8-86.4 86.4-86.4 14.6 0 38.3 16 89.6 16 51.7 0 74.9-16 89.6-16 47.6 0 86.4 38.8 86.4 86.4V464zM224 288c79.5 0 144-64.5 144-144S303.5 0 224 0 80 64.5 80 144s64.5 144 144 144zm0-240c52.9 0 96 43.1 96 96s-43.1 96-96 96-96-43.1-96-96 43.1-96 96-96z" viewBox="0 0 448 512" />
                </div>
                
            </footer>
        </div>
    )
}

export default App;